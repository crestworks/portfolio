<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Project;
use App\Category;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        $categories = Category::all();

        return view('project.index')->withProjects($projects)->withCategories($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('project.create')->withCategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'title' => 'required|max:50',
            'category_id' => 'required|integer',
            'description' => 'required|max:500',
            'source' => 'nullable|url',
            'view' => 'nullable|url'
        ));

        // store in the database
        $project = new Project;

        $project->title = $request->title;
        $project->category_id = $request->category_id;
        $project->description = $request->description;
        $project->source = $request->source;
        $project->view = $request->view;

        $project->save();

        Session::flash('success', 'A new Project has been added!');

        // redirect
        return redirect()->route('project.show', $project->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);
        $categories = Category::all();

        return view('project.show')->withProject($project)->withCategories($categories);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        $categories = Category::all();

        return view('project.edit')->withProject($project)->withCategories($categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate data
        $this->validate($request, array(
            'title' => 'required|max:50',
            'category_id' => 'required|integer',
            'description' => 'required|max:500',
            'source' => 'nullable|url',
            'view' => 'nullable|url'
        ));

        // Store data
        $project = Project::find($id);

        $project->title = $request->title;
        $project->category_id = $request->category_id;
        $project->description = $request->description;
        $project->source = $request->source;
        $project->view = $request->view;

        $project->save();

        Session::flash('success', 'This Project has been updated!');

        // Redirect
        return redirect()->route('project.show', $project->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        
        $project->delete();
        
        Session::flash('success', 'The Project was successfully deleted!');
        
        return redirect()->route('project.index');
    }
}
