<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Book;
use App\Resource;
use App\Project;
use App\Category;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        $resources = Resource::all();
        $projects = Project::all();
        $categories = Category::all();

        $books_count = $books->count();
        $resources_count = $resources->count();
        $projects_count = $projects->count();
        $categories_count = $categories->count();

        /*
         * Customize Categories object
         * loop through the categories object
         * count the number of projects for each category
         * add new Key Value pair to categories object
         * 'projects' = 'number of projects for the category'
         */

        for ($x = 0; $x < $categories_count; $x++)
        {
            $categories[$x]['projects'] = $projects->where('category_id', '=', $categories[$x]['id'])->count();
        }
        
        return view('dashboard')
            ->withBooks_count($books_count)
            ->withResources_count($resources_count)
            ->withProjects($projects)
            ->withCategories($categories);
    }
}
