<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Category;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'category' => 'required|max:50'
        ));

        // store in the database
        $category = new Category;

        $category->category = $request->category;

        $category->save();

        Session::flash('success', 'A new Project has been added!');

        // redirect
        return redirect()->route('project.index');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);

        return view('category.edit')->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate data
        $this->validate($request, array(
            'category' => 'required|max:50'
        ));

        // Store data
        $category = Category::find($id);

        $category->category = $request->category;

        $category->save();

        Session::flash('success', 'The Category has been updated!');

        // redirect
        return redirect()->route('project.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        
        $category->delete();
        
        Session::flash('success', 'The Category was successfully deleted!');
        
        return redirect()->route('project.index');
    }
}
