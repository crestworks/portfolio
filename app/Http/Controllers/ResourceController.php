<?php

namespace App\Http\Controllers;

use App\Resource;
use Illuminate\Http\Request;

use Session;

class ResourceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = Resource::all();

        return view('resource.index')->withResources($resources);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('resource.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'name' => 'required|max:100',
            'link' => 'required|url'
        ));

        // store in the database
        $resource = new Resource;

        $resource->name = $request->name;
        $resource->link = $request->link;

        $resource->save();

        Session::flash('success', 'A new Resource has been added!');

        // redirect
        return redirect()->route('resource.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resource = Resource::find($id);

        return view('resource.edit')->withResource($resource);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate data
        $this->validate($request, array(
            'name' => 'required|max:100',
            'link' => 'required|url'
        ));

        // Store data
        $resource = Resource::find($id);

        $resource->name = $request->name;
        $resource->link = $request->link;

        $resource->save();

        Session::flash('success', 'The Resource has been updated!');

        // redirect
        return redirect()->route('resource.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resource = Resource::find($id);
        
        $resource->delete();
        
        Session::flash('success', 'The Resource was successfully deleted!');
        
        return redirect()->route('resource.index');
    }
}
