<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

use Session;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();

        return view('book.index')->withBooks($books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'title' => 'required|max:100',
            'author' => 'required|max:100'
        ));

        // store in the database
        $book = new Book;

        $book->title = $request->title;
        $book->author = $request->author;

        $book->save();

        Session::flash('success', 'A new Book has been added!');

        // redirect
        return redirect()->route('book.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);

        return view('book.edit')->withBook($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate data
        $this->validate($request, array(
            'title' => 'required|max:100',
            'author' => 'required|max:100'
        ));

        // Store data
        $book = Book::find($id);

        $book->title = $request->title;
        $book->author = $request->author;

        $book->save();

        Session::flash('success', 'The Book has been updated!');

        // redirect
        return redirect()->route('book.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        
        $book->delete();
        
        Session::flash('success', 'The Book was successfully deleted!');
        
        return redirect()->route('book.index');
    }
}
