<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Mail;

use App\Book;
use App\Resource;
use App\Project;
use App\Category;
use GuzzleHttp\Client;

class WelcomeController extends Controller
{
    public function welcome()
    {
        $books = Book::all();
        $resources = Resource::all();
        $projects = Project::all();
        $categories = Category::all();

        return view('portfolio.welcome')
            ->withBooks($books)
            ->withResources($resources)
            ->withProjects($projects)
            ->withCategories($categories);
    }

    public function email(Request $request)
	{
		$this->validate($request, array(
			'name' => 'required|min:3',
			'email' => 'required|email',
			'subject' => 'required',
			'body' => 'required|min:10',
		));

		$token = $request->input('g-recaptcha-response');

		// dd($token);

		if ($token) {
			$client = new Client();
			
			$response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
				'form_params' => array(
					'secret'    => env('RECAPTCHA_SECRET'),
					'response'  => $token
					)
				]);
					
			$results = json_decode($response->getBody()->getContents());
			
			if ($results->success) {
				// replace the below address with your own
				// otherwise I may get all your messages
				$send = 'nick@crestworks.io';

				$data = array(
					'name' => $request->name,
					'email' => $request->email,
					'send' => $send,
					'subject' => $request->subject,
					'body' => $request->body,
				);

				Mail::send('emails.contact', $data, function($message) use ($data)
				{
					$message->to($data['send'])
							->replyTo($data['email'], $data['name'])
							->subject($data['subject']);
				});

				Session::flash('success', 'Your Message was successfully sent!');

      // redirect
      return redirect()->back();
			} else {
				Session::flash('error', 'You are probably a robot!');
				// redirect
				return redirect()->back();
			}
		} else {
			// redirect
			return redirect()->back();
		}
	}
}
