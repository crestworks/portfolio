## About

I am an aspiring web developer. I have been self-learning since 2015. This repository is my personal Portfolio of projects, resources I use (and still want to learn), and books I have read and recommend to others. I created it not simply to be a static website but a way to continuously update some of the contents.

This project is open-source and anyone is allowed to view the code and clone it. I used the following resources:
- Laravel (PHP framework)
- Bootstrap (CSS framework)
- MySQL (database)

## Laravel License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## Bootstrap Copyright and License

Code and documentation copyright 2011-2018 the Bootstrap Authors and Twitter, Inc. Code released under the MIT License. Docs released under Creative Commons.