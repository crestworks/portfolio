{{-- <!DOCTYPE html>
<html>

  @include('partials._head')

  <body>

    @include('portfolio.hero')

    @include('portfolio.about')
    
    @include('portfolio.projects')

    @include('portfolio.books')

    @include('portfolio.resources')

    @include('portfolio.contact')

    @include('partials.portfolio._footer')

  </body>
</html>
 --}}
<!doctype html>
<html lang="en">

	@include('partials._head')

  <body>

    <main>

      @yield('content')
      
      @include('partials.portfolio._footer')

		</main>

  </body>
</html>