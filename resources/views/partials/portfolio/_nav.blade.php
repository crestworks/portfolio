<nav class="navbar navbar-expand-md navbar-dark pt-4 pl-0 pr-0 index-2">
  {{-- <div class="container"> --}}
    <a class="navbar-brand" href="/">CrestWorks</a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="#about">About Me</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#projects">Projects</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#books">Books</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#resources">Resources</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#contact">Contact</a>
        </li>
      </ul>
    </div>
  {{-- </div> --}}
</nav>

@section('scripts')
<script>
  $(document).ready(function(){

    var $window = $(window).width();
    var landingProfileImage = 1;

    function slideDown() {
      if(landingProfileImage == 1) {
        $(".landing-profile-pic").addClass("slide-down");
        landingProfileImage = 0;
      } else {
        $(".landing-profile-pic").removeClass("slide-down");
        landingProfileImage = 1;
      }
    }

    function checkWidth() {
      if(($window > 768) && !($(".navbar-toggler").hasClass("collapsed"))) {
        $(".landing-profile-pic").removeClass("slide-down");
        landingProfileImage = 1;
        $('.collapse').collapse('hide');
      }
    }
    
    $(".navbar-toggler").click(slideDown);

    // Check the window width after resize and run function
    $(window).resize(checkWidth);
  });
</script>
@stop