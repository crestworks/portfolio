<footer class="container-fluid bg-light mt-4">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="text-center mt-2 mb-2"> CrestWorks <i class="fas fa-copyright"></i></p>
      </div>
    </div>
  </div>
</footer>

@yield('scripts')