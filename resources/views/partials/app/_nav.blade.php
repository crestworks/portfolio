<nav class="navbar navbar-expand-md navbar-light pt-4">
  <div class="container">
    <a class="navbar-brand" href="/">CrestWorks</a>
    <p class="m-0">{{ Auth::check() ? Auth::user()->name : '' }}</p>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        @if(Auth::check())
        <li class="nav-item">
          <a class="nav-link" href="{{ route('dashboard') }}">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('project.index') }}">Projects</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('book.index') }}">Books</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('resource.index') }}">Resources</a>
        </li>
        <li class="nav-item">
          <form action="{{ url('logout') }}" method="POST">
            {{ csrf_field() }}
            <button class="nav-link">Logout</button>
          </form>
        </li>
        @else
        <li class="nav-item">
          <a class="nav-link" href="/">Landing Page</a>
        </li>
        @endif
      </ul>
    </div>
  </div>
</nav>