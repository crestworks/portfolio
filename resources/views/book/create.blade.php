@extends('main')

@section('title', '| Create Book')

@section('content')

<div class="container mt-4 pb-80">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Create Book</h3>
        <a href="{{ route('book.index') }}" class="btn-icon-primary lead"><i class="fas fa-times"></i></a>
      </div>
      <hr>
      <form action="{{ url('book') }}" method="POST">

        {{ csrf_field() }}

        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" name="title" placeholder="Book title" value="{{ old('title') }}">
        </div>
        <div class="form-group">
          <label for="author">Author</label>
          <input type="text" class="form-control" name="author" placeholder="Book author" value="{{ old('author') }}">
      </div>
        <div class="form-group">
          <button type="submit" class="btn btn-block btn-outline-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

@stop