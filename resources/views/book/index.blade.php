@extends('main')

@section('title', '| Books')

@section('content')

<div class="container mt-4 pb-80">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Books</h3>
        <div>  
          {{-- <a href="{{ route('dashboard') }}" class="btn-icon-primary lead mr-2"><i class="fas fa-arrow-left"></i></a> --}}
          <a href="{{ route('book.create') }}" class="btn-icon-primary lead"><i class="fas fa-plus"></i></a>
        </div>
      </div>
      <hr>
      <ul class="list-group list">
        @foreach($books as $book)
        <li class="list-group-item d-flex justify-content-between align-items-center list-item">
          <div>
            <p class="mb-1">{{ $book->title }}</p>
            <small>- <em>{{ $book->author }}</em></small>
          </div>
          <a href="{{ route('book.edit', $book->id) }}" class="list-item-link"><i class="fas fa-edit"></i></a>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>

@stop