@extends('main')

@section('title', '| Register')

@section('content')

<div class="container mt-4">
  <div class="row pt-4">
    <div class="col-md-4 offset-md-4">
      <div class="card">
        <div class="card-header">
          <p class="card-subtitle p-0 m-0">Register</p>
        </div>
        <div class="card-body">
          <form method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">Name</label>
              <input type="text" class="form-control" name="name" placeholder="Firstname Last" value="{{ old('name') }}" required autofocus>
              @if ($errors->has('name'))
                <span class="form-text text-muted">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email">Email</label>
              <input type="email" class="form-control" name="email" placeholder="Email address" value="{{ old('email') }}" required>
              @if ($errors->has('email'))
                <span class="form-text text-muted">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password">Password</label>
              <input type="password" class="form-control" name="password" placeholder="Enter password" required>
              @if ($errors->has('password'))
                <span class="form-text text-muted">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="password-confirm">Confirm Password</label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Password again" required>
            </div>

            <button type="submit" class="btn btn-outline-primary btn-block">Register</button>

          </form>  
        </div>
      </div>
    </div>
  </div>
</div>

@stop
