@extends('main')

@section('title', '| Login')

@section('content')

<div class="container mt-4">
  <div class="row pt-4">
    <div class="col-md-4 offset-md-4">
      <div class="card">
        <div class="card-header">
          <p class="card-subtitle p-0 m-0">Login</p>
        </div>
        <div class="card-body">
          <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email">Email</label>
              <input type="email" class="form-control" name="email" placeholder="Email address" value="{{ old('email') }}" required autofocus>
              @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password">Password</label>
              <input type="password" class="form-control" name="password" placeholder="Enter password" required>
              @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <div class="">
                <div class="checkbox">
                  <label class="m-0">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                  </label>
                </div>
              </div>
            </div>

            <button type="submit" class="btn btn-outline-primary btn-block">Submit</button>

            <div class="form-group mb-0 mt-2">
              <a class="card-link" href="{{ route('password.request') }}">
                Forgot Your Password?
              </a>
            </div>

          </form>  
        </div>
      </div>
    </div>
  </div>
</div>

@stop