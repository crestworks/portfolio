@extends('main')

@section('title', '| Create Resource')

@section('content')

<div class="container mt-4 pb-80">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Create Resource</h3>
        <a href="{{ route('resource.index') }}" class="btn-icon-primary lead"><i class="fas fa-times"></i></a>
      </div>
      <hr>
      <form action="{{ url('resource') }}" method="POST">

        {{ csrf_field() }}

        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name" placeholder="Name of resource" value="{{ old('resource') }}">
        </div>
        <div class="form-group">
          <label for="link">Link</label>
          <input type="text" class="form-control" name="link" placeholder="Link to resource" value="{{ old('link') }}">
      </div>
        <div class="form-group">
          <button type="submit" class="btn btn-block btn-outline-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

@stop