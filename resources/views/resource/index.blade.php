@extends('main')

@section('title', '| Resources')

@section('content')

<div class="container mt-4 pb-80">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Resources</h3>
        <div>  
          {{-- <a href="{{ route('dashboard') }}" class="btn-icon-primary lead mr-2"><i class="fas fa-arrow-left"></i></a> --}}
          <a href="{{ route('resource.create') }}" class="btn-icon-primary lead"><i class="fas fa-plus"></i></a>
        </div>
      </div>
      <hr>
      <ul class="list-group list">
        @foreach($resources as $resource)
        <li class="list-group-item d-flex justify-content-between align-items-center list-item">
          <div>
            <p class="mb-1">{{ $resource->name }}</p>
            <a href="{{ $resource->link }}" class="list-item-link"><small>Resource Link</small></a>
          </div>
          <a href="{{ route('resource.edit', $resource->id) }}" class="list-item-link"><i class="fas fa-edit"></i></a>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>

@stop