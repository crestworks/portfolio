@extends('main')

@section('title', '| Edit Resource')

@section('content')

<div class="container mt-4 pb-80">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Edit Resource</h3>
        <div>
          <button type="button" class="btn-icon-danger lead mr-2" data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash-alt"></i></button>
          <a href="{{ route('resource.index') }}" class="btn-icon-primary lead"><i class="fas fa-times"></i></a>
        </div>
      </div>
      <hr>
      <form action="{{ url('resource', $resource->id) }}" method="POST">

        {{ method_field('PATCH') }}
        {{ csrf_field() }}

        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name" placeholder="Resource name" value="{{ $resource->name }}">
        </div>
        <div class="form-group">
          <label for="link">Link</label>
          <input type="text" class="form-control" name="link" placeholder="Link to resource" value="{{ $resource->link }}">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-block btn-outline-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Delete</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this Resource?</p>
          <form action="{{ url('resource', $resource->id) }}" method="POST">
  
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
  
            <button type="submit" class="btn btn-danger btn-block">Delete</button>
          </form>
        </div>
      </div>
    </div>
  </div>

@stop