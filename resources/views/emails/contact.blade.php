{{--
  |--------------------------------------------------------------------------
  | Email Markup
  |--------------------------------------------------------------------------
  |
  | Use the file below to write the markup for the email you will receive
  | when someone visits your portfolio site.
  | The below markup is basic because this is not directed at the public.
  | --}}


<h3>CrestWorks</h3>

<p>You received this message from the CrestWorks website</p>

<p>Name: {{ $name }}</p>
<p>From: {{ $email }}</p>
<p>Subject: {{ $subject }}</p>
<p>Message: {{ $body }}</p>
