<!doctype html>
<html lang="en">

	@include('partials._head')

  <body>

  	@include('partials.app._nav')

    <main class="container-fluid">

      @include('partials.app._messages')

			@yield('content')

		</main>

    @include('partials.app._footer')

  </body>
</html>
