@extends('main')

@section('title', '| Dashboard')

@section('content')

<div class="container mt-4">
  <div class="row pt-4">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          <p class="card-subtitle p-0 m-0">Projects</p>
        </div>
        <div class="card-body">
          @foreach($categories as $category)
          <div class="d-flex justify-content-between align-items-center">
            <p>{{ $category->category }}</p>
          <span class="badge badge-primary badge-pill">{{ $category->projects }}</span>
          </div>
          @endforeach
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          <p class="card-subtitle p-0 m-0">Books</p>
        </div>
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-center">
            <p>Recommended Books</p>
            <span class="badge badge-primary badge-pill">{{ $books_count }}</span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          <p class="card-subtitle p-0 m-0">Resources</p>
        </div>
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-center">
            <p>Resources Used</p>
            <span class="badge badge-primary badge-pill">{{ $resources_count }}</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop