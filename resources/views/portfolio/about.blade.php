<div class="container">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <h3 id="about">About Me</h3>
      <hr class="header-line">
      <p>
          I first stumbled into the basics of HTML during an information systems class at university. 
          I'm not really sure what the rest of the class covered because I was so fascinated with this one topic. 
          From there I started my web-dev journey taking course after course on Udemy, YouTube videos, blogs, and of course StackExchange.
      </p>
      <p>
          Over the years I settled on focusing my energy on mastering only a handful of languages and technologies. 
          I did this because...
        <blockquote class="blockquote">
          <p class="mb-0">If I kept changing to the latest and greatest flavor-of-the-month I would never gain the necessary proficiency of a professional.</p>
        </blockquote>
        So I took as step back and focused on what I was getting comfortable with. 
        Now I am working on mastering PHP as my server-side language, as it stands I am very proficient here. 
        I like to use Laravel as my PHP framework. 
        My database of choice is a MySQL (relational database), because I found it easier to move from Microsoft Excel in the beginning. 
        For my frontend I use jQuery, as the JS library, and Bootstrap for the CSS framework. 
        Bootstrap requires the use of jQuery anyway.
      </p>
      <p>
        As far as professional education goes I have an Aviation Bachelors from Henderson State University, as well as an MBA from the same University
      </p>
    </div>
  </div>
</div>