@extends('welcome')

@section('title', '| My Portfolio')

@section('content')

<div class="container-fluid landing-hero landing-size">
  
  {{-- Hero Section --}}
  <div class="container d-flex flex-column justify-content-between landing-size no-overflow">

    {{-- Nav Section --}}
    @include('partials.portfolio._nav')

    <div class="row pb-80">
      <div class="col-md-12 hero-area">
        <img class="landing-profile-pic" src="{{ asset('images/profile_pic1.jpg')}}">
        <div class="hero-text">
          <h1>Nick James</h1>
          <h2>Web Design &amp; Development in Zimbabwe</h2>
          <p class="lead">
            I enjoy the problem solving and creativity of both backend and frontend programming.
          </p>
          <h2>
          <p class="lead">Creating a Personal Portfolio page started out as a project for <a href="https://freecodecamp.org/">freecodecamp</a>.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid d-flex justify-content-center see-more">
  <i class="fas fa-arrow-down"></i>
</div>

{{-- About Section --}}
<div class="container">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <h3 id="about">About Me</h3>
      <hr class="header-line">
      <p>
          I first stumbled into the basics of HTML during an information systems class at university. 
          I'm not really sure what the rest of the class covered because I was so fascinated with this one topic. 
          From there I started my web-dev journey taking course after course on Udemy, YouTube videos, blogs, and of course StackExchange.
      </p>
      <p>
          Over the years I settled on focusing my energy on mastering only a handful of languages and technologies. 
          I did this because...
        <blockquote class="blockquote">
          <p class="mb-0">If I kept changing to the latest and greatest flavor-of-the-month I would never gain the necessary proficiency of a professional.</p>
        </blockquote>
        So I took as step back and focused on what I was getting comfortable with. 
        Now I am working on mastering PHP as my server-side language, as it stands I am very proficient here. 
        I like to use Laravel as my PHP framework. 
        My database of choice is a MySQL (relational database), because I found it easier to move from Microsoft Excel in the beginning. 
        For my frontend I use jQuery, as the JS library, and Bootstrap for the CSS framework. 
        Bootstrap requires the use of jQuery anyway.
      </p>
      <p>
        As far as professional education goes I have an Aviation Bachelors from Henderson State University, as well as an MBA from the same University
      </p>
    </div>
  </div>
</div>

{{-- Projects Section --}}
<div class="container">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <h3 id="projects">Projects</h3>
      <hr class="header-line">
      <p>
          My Projects are not extensive. 
          The following list encompasses all the work I have done, separated into 3 categories: those required by FreeCodeCamp, Personal Projects, Professional Projects, and some Projects In-Progress.
      </p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 card-columns">
      @foreach($projects as $project)
      <div class="card {{ $project->category_id == 1 ? "website-card-freecodecamp" : ($project->category_id == 2 ? "website-card-personal" : ($project->category_id == 3 ? "website-card-professional" : "website-card-progress")) }}">
        <div class="card-body">
          <h5 class="card-title">{{ $project->title }}</h5>
          <h6 class="card-subtitle">{{ $project->category->category }}</h6>
          <p class="card-text">{{ $project->description }}</p>
          @if(isset($project->source))
          <a href="{{ $project->source }}" class="card-link">Source Code</a>
          @endif
          @if(isset($project->view))
          <a href="{{ $project->view }}" class="card-link">View</a>
          @endif
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

{{-- Books Section --}}
<div class="container">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <h3 id="books">Books</h3>
      <hr class="header-line">
      <p>
        I will be honest I have never been a book worm but here are the greats everyone should digest...
      </p>
      <ul class="list-group list">
        @foreach($books as $book)
        <li class="list-group-item d-flex justify-content-between align-items-center list-item">
          <p class="mb-1">{{ $book->title }}</p>
          <small>- <em>{{ $book->author }}</em></small>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>

{{-- Resources Section --}}
<div class="container">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <h3 id="resources">Resources</h3>
      <hr class="header-line">
      <p>
        Here is a list of my favorite resources I use on projects, and some resources that I am yet to learn (and then master).
      </p>
      <ul class="list-group list">
        @foreach($resources as $resource)
        <li class="list-group-item d-flex justify-content-between align-items-center list-item">
          <p class="mb-1">{{ $resource->name }}</p>
          <a href="{{ $resource->link }}" class="list-item-link"><small>Resource Link</small></a>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>

{{-- Contact Section --}}
<div class="container">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <h3 id="contact">Contact Me</h3>
      <hr class="header-line">
      <p>
        There are a few ways to get in touch with me, but the best way is to send me an email.
      </p>
      <nav>
        <div class="nav contact-methods d-flex justify-content-between"  id="nav-tab" role="tablist">
          <a class="nav-item active d-flex justify-content-center align-items-center contact-method" id="nav-email-tab" data-toggle="tab" href="#nav-email" role="tab" aria-controls="nav-email" aria-selected="true"><i class="fas fa-envelope"></i></a>
          <a class="nav-item d-flex justify-content-center align-items-center contact-method" id="nav-linked-tab" data-toggle="tab" href="#nav-linkedin" role="tab" aria-controls="nav-linkedin" aria-selected="false"><i class="fab fa-linkedin-in"></i></a>
          <a class="nav-item d-flex justify-content-center align-items-center contact-method" id="nav-bit-bucket-tab" data-toggle="tab" href="#nav-bit-bucket" role="tab" aria-controls="nav-bit-bucket" aria-selected="false"><i class="fab fa-bitbucket"></i></a>
          <a class="nav-item d-flex justify-content-center align-items-center contact-method" id="nav-phone-tab" data-toggle="tab" href="#nav-phone" role="tab" aria-controls="nav-phone" aria-selected="false"><i class="fas fa-phone"></i></a>
        </div>
      </nav>
      <div class="tab-content mt-4" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-email" role="tabpanel" aria-labelledby="nav-email-tab">

          @include('partials.portfolio._messages')

          <form action="{{ route('email') }}" method="POST" id="form">

            {{ csrf_field() }}

            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" aria-describedby="name" placeholder="Full name" value="{{ old('name') }}" {{ $errors->has('name') ? 'autofocus' : '' }}>
  
              @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="email">Email Address</label>
              <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" aria-describedby="email" placeholder="Enter email" value="{{ old('email') }}" {{ $errors->has('email') ? 'autofocus' : '' }}>
              <small id="emailHelp" class="form-text text-muted">Your email address will not stored, only used for replying.</small>

              @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="subject">Subject</label>
              <select class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" {{ $errors->has('subject') ? 'autofocus' : '' }}>
                <option>Select</option>
                <option {{ old('subject') == "Project Proposal" ? "selected" : "" }} value="Project Proposal">Project Proposal</option>
                <option {{ old('subject') == "Job Offer" ? "selected" : "" }} value="Job Offer">Job Offer</option>
                <option {{ old('subject') == "Advice" ? "selected" : "" }} value="Advice">Advice</option>
                <option {{ old('subject') == "General Conversation" ? "selected" : "" }} value="General Conversation">General Conversation</option>
                <option {{ old('subject') == "Feedback" ? "selected" : "" }} value="Feedback">Feedback</option>
                <option {{ old('subject') == "Something else..." ? "selected" : "" }} value="Something else...">Something else...</option>
              </select>

              @if ($errors->has('subject'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('subject') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="body">Message</label>
              <textarea class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" aria-describedby="body" placeholder="Write your message here..." {{ $errors->has('body') ? 'autofocus' : '' }}>{{ old('body') }}</textarea>
            
              @if ($errors->has('body'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('body') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="recapture">Recapture</label>
              <div class="g-recaptcha" data-sitekey="6LeCWIkUAAAAAExwOmlkQnendy-JXA9WLN6mkY2t"></div>

              @if ($errors->has('g-recaptcha-response'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                </span>
              @endif
            </div>

            <button type="submit" class="btn btn-outline-primary btn-block">Submit</button>
          </form>
        </div>
        <div class="tab-pane fade" id="nav-linkedin" role="tabpanel" aria-labelledby="nav-linkedin-tab">
          <p>
            Click on the link below and you will be redirected to my LinkedIn page.
            Ideally you will use this to reach out to me, and not endlessly scroll through my stuff.
          </p>

          {{-- Make sure you do not use this link --}}
          <a href="https://www.linkedin.com/in/nicholas-james-mba" class="btn btn-outline-primary btn-block">LinkedIn</a>

        </div>
        <div class="tab-pane fade" id="nav-bit-bucket" role="tabpanel" aria-labelledby="nav-bit-bucket-tab">
          <p>
              I am a big fan of Bit Bucket because of the unlimited number of private repositories I can make simply on a free account. 
              If you are a student be sure to use your student email address to sign up and your account will automatically be upgraded.
          </p>
          <p>
            Feel free to check out my public repositories, make some comments, and feel free to contribute.
          </p>

          {{-- Make sure you do not use this link --}}
          <a href="https://bitbucket.org/crestworks/" class="btn btn-outline-primary btn-block">Bit Bucket</a>

        </div>
        <div class="tab-pane fade" id="nav-phone" role="tabpanel" aria-labelledby="nav-phone-tab">
          <p>If you really need to get in touch with me urgently here is my phone number:</p>

          {{-- Make sure you do not use this phone number --}}
          <p class="lead text-center">+263 (0) 784-536-128</p>

          <small>Because I live in Zimbabwe the number above starts with the international code for Zimbabwe (+263).</small>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>

$(function() {
  $('#form').submit(function(event) {
    var verified = grecaptcha.getResponse();

    if(verified.length === 0) {
      event.preventDefault();
    }
  });
});

</script>
@endsection