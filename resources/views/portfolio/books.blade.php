<div class="container">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <h3 id="books">Books</h3>
      <hr class="header-line">
      <p>
        I will be honest I have never been a book worm but here are the greats everyone should digest...
      </p>
      <ul class="list-group list">
        @foreach($books as $book)
        <li class="list-group-item d-flex justify-content-between align-items-center list-item">
          <p class="mb-1">{{ $book->title }}</p>
          <small>- <em>{{ $book->author }}</em></small>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>