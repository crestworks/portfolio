<div class="container">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <h3 id="contact">Contact Me</h3>
      <hr class="header-line">
      <p>
        There are a few ways to get in touch with me, but the best way is to send me an email.
      </p>
      <nav>
        <div class="nav contact-methods d-flex justify-content-between"  id="nav-tab" role="tablist">
          <a class="nav-item active d-flex justify-content-center align-items-center contact-method" id="nav-email-tab" data-toggle="tab" href="#nav-email" role="tab" aria-controls="nav-email" aria-selected="true"><i class="fas fa-envelope"></i></a>
          <a class="nav-item d-flex justify-content-center align-items-center contact-method" id="nav-linked-tab" data-toggle="tab" href="#nav-linkedin" role="tab" aria-controls="nav-linkedin" aria-selected="false"><i class="fab fa-linkedin-in"></i></a>
          <a class="nav-item d-flex justify-content-center align-items-center contact-method" id="nav-bit-bucket-tab" data-toggle="tab" href="#nav-bit-bucket" role="tab" aria-controls="nav-bit-bucket" aria-selected="false"><i class="fab fa-bitbucket"></i></a>
          <a class="nav-item d-flex justify-content-center align-items-center contact-method" id="nav-phone-tab" data-toggle="tab" href="#nav-phone" role="tab" aria-controls="nav-phone" aria-selected="false"><i class="fas fa-phone"></i></a>
        </div>
      </nav>
      <div class="tab-content mt-4" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-email" role="tabpanel" aria-labelledby="nav-email-tab">
          <form action="{{ route('email') }}" method="POST">

            {{ csrf_field() }}

            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" aria-describedby="name" placeholder="Full name" value="{{ old('name') }}" {{ $errors->has('name') ? 'autofocus' : '' }}>
  
              @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="email">Email Address</label>
              <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" aria-describedby="email" placeholder="Enter email" value="{{ old('email') }}" {{ $errors->has('email') ? 'autofocus' : '' }}>
              <small id="emailHelp" class="form-text text-muted">Your email address will not stored, only used for replying.</small>

              @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="subject">Subject</label>
              <select class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" {{ $errors->has('subject') ? 'autofocus' : '' }}>
                <option>Select</option>
                <option {{ old('subject') == "Project Proposal" ? "selected" : "" }} value="Project Proposal">Project Proposal</option>
                <option {{ old('subject') == "Job Offer" ? "selected" : "" }} value="Job Offer">Job Offer</option>
                <option {{ old('subject') == "Advice" ? "selected" : "" }} value="Advice">Advice</option>
                <option {{ old('subject') == "General Conversation" ? "selected" : "" }} value="General Conversation">General Conversation</option>
                <option {{ old('subject') == "Feedback" ? "selected" : "" }} value="Feedback">Feedback</option>
                <option {{ old('subject') == "Something else..." ? "selected" : "" }} value="Something else...">Something else...</option>
              </select>

              @if ($errors->has('subject'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('subject') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="body">Message</label>
              <textarea class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" aria-describedby="body" placeholder="Write your message here..." {{ $errors->has('body') ? 'autofocus' : '' }}>{{ old('body') }}</textarea>
            
              @if ($errors->has('body'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('body') }}</strong>
                </span>
              @endif
            </div>

            <button type="submit" class="btn btn-outline-primary btn-block">Submit</button>
          </form>
        </div>
        <div class="tab-pane fade" id="nav-linkedin" role="tabpanel" aria-labelledby="nav-linkedin-tab">
          <p>
            Click on the link below and you will be redirected to my LinkedIn page.
            Ideally you will use this to reach out to me, and not endlessly scroll through my stuff.
          </p>

          {{-- Make sure you do not use this link --}}
          <a href="https://www.linkedin.com/in/nicholas-james-mba" class="btn btn-outline-primary btn-block">LinkedIn</a>

        </div>
        <div class="tab-pane fade" id="nav-bit-bucket" role="tabpanel" aria-labelledby="nav-bit-bucket-tab">
          <p>
              I am a big fan of Bit Bucket because of the unlimited number of private repositories I can make simply on a free account. 
              If you are a student be sure to use your student email address to sign up and your account will automatically be upgraded.
          </p>
          <p>
            Feel free to check out my public repositories, make some comments, and feel free to contribute.
          </p>

          {{-- Make sure you do not use this link --}}
          <a href="https://bitbucket.org/crestworks/" class="btn btn-outline-primary btn-block">Bit Bucket</a>

        </div>
        <div class="tab-pane fade" id="nav-phone" role="tabpanel" aria-labelledby="nav-phone-tab">
          <p>If you really need to get in touch with me urgently here is my phone number:</p>

          {{-- Make sure you do not use this phone number --}}
          <p class="lead text-center">+263 (0) 784-536-128</p>

          <small>Because I live in Zimbabwe the number above starts with the international code for Zimbabwe (+263).</small>
        </div>
      </div>
    </div>
  </div>
</div>