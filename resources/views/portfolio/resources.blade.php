<div class="container">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <h3 id="resources">Resources</h3>
      <hr class="header-line">
      <p>
        Here is a list of my favorite resources I use on projects, and some resources that I am yet to learn (and then master).
      </p>
      <ul class="list-group list">
        @foreach($resources as $resource)
        <li class="list-group-item d-flex justify-content-between align-items-center list-item">
          <p class="mb-1">{{ $resource->name }}</p>
          <a href="{{ $resource->link }}" class="list-item-link"><small>Resource Link</small></a>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>