@section('title', '')

<div class="container-fluid landing-hero landing-size">
  
  <div class="container d-flex flex-column justify-content-between landing-size no-overflow">

    @include('partials.portfolio._nav')

    <div class="row pb-80">
      <div class="col-md-12 hero-area">
        <img class="landing-profile-pic" src="{{ asset('images/profile_pic1.jpg')}}">
        <div class="hero-text">
          <h1>Nick James</h1>
          <p class="lead">
            I have been learning the art of web development since 2015.<br>
            I enjoy the problem solving and creativity of both backend and frontend programming.
          </p>
          <p class="lead">Creating a Personal Portfolio page started out as a project for <a href="https://freecodecamp.org/">freecodecamp</a>.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid d-flex justify-content-center see-more">
  <i class="fas fa-arrow-down"></i>
</div>