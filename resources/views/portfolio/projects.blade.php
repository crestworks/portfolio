<div class="container">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <h3 id="projects">Projects</h3>
      <hr class="header-line">
      <p>
          My Projects are not extensive. 
          The following list encompasses all the work I have done, separated into 3 categories: those required by FreeCodeCamp, Personal Projects, Professional Projects, and some Projects In-Progress.
      </p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 card-columns">
      @foreach($projects as $project)
      <div class="card {{ $project->category_id == 1 ? "website-card-freecodecamp" : ($project->category_id == 2 ? "website-card-personal" : ($project->category_id == 3 ? "website-card-professional" : "website-card-progress")) }}">
        <div class="card-body">
          <h5 class="card-title">{{ $project->title }}</h5>
          <h6 class="card-subtitle">{{ $project->category->category }}</h6>
          <p class="card-text">{{ $project->description }}</p>
          @if(isset($project->source))
          <a href="{{ $project->source }}" class="card-link">Source Code</a>
          @endif
          @if(isset($project->view))
          <a href="{{ $project->view }}" class="card-link">View</a>
          @endif
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>