@extends('main')

@section('title', '| Show Project')

@section('content')

<div class="container mt-4 pb-80">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Show Project</h3>
        <div>
          <button type="button" class="btn-icon-danger lead mr-2" data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash-alt"></i></button>
          <a href="{{ route('project.edit', $project->id) }}" class="btn-icon-primary lead mr-2"><i class="fas fa-edit"></i></a>
          <a href="{{ route('project.index') }}" class="btn-icon-primary lead"><i class="fas fa-times"></i></a>
        </div>
      </div>
      <hr>
      <form>
        <div class="form-group">
          <label for="title">Title</label>
          <p readonly class="form-control-plaintext">{{ $project->title }}</p>
        </div>
        <div class="form-group">
          <label for="category_id">Category</label>
          <p readonly class="form-control-plaintext">{{ $project->category->category }}</p>
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <p readonly class="form-control-plaintext">{{ $project->description }}</p>
        </div>
        <div class="form-group">
          <label for="source">Source Code</label>
          <a readonly class="form-control-plaintext link-primary" href="{{ $project->source }}">{{ $project->source }}</a>
        </div>
        <div class="form-group">
          <label for="view">View</label>
          <a readonly class="form-control-plaintext link-primary" href="{{ $project->view }}">{{ $project->view }}</a>
        </div>
        <div class="form-group d-flex justify-content-between">
          <small>{{ date('M j, Y H:i', strtotime($project->created_at)) }}</small>
          <small>{{ date('M j, Y H:i', strtotime($project->updated_at)) }}</small>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this Project?</p>
        <form action="{{ url('project', $project->id) }}" method="POST">

          {{ method_field('DELETE') }}
          {{ csrf_field() }}

          <button type="submit" class="btn btn-danger btn-block">Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>

@stop