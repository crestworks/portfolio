@extends('main')

@section('title', '| Projects')

@section('content')

<div class="container mt-4 pb-80">
  <div class="row">
    <div class="col-md-4 offset-md-1 mb-4">
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Categories</h3>
        <div>  
          {{-- <a href="{{ route('dashboard') }}" class="btn-icon-primary lead mr-2"><i class="fas fa-arrow-left"></i></a> --}}
          <a href="{{ route('category.create') }}" class="btn-icon-primary lead"><i class="fas fa-plus"></i></a>
        </div>
      </div>
      <hr>
      <ul class="list-group">
        @foreach($categories as $category)
        <li class="d-flex justify-content-between list-group-item {{ $category->id == 1 ? "category-list-freecodecamp" : ($category->id == 2 ? "category-list-personal" : ($category->id == 3 ? "category-list-professional" : "category-list-progress")) }}">
          {{ $category->category }}
          <a class="{{ $category->id == 1 ? "category-list-btn-freecodecamp" : ($category->id == 2 ? "category-list-btn-personal" : ($category->id == 3 ? "category-list-btn-professional" : "category-list-btn-progress")) }}" href="{{ route('category.edit', $category->id) }}"><i class="fas fa-edit"></i></a>
        </li>
        @endforeach
      </ul>
      <small>Please Note: that if you create a new category you will have to create the custom css too.</small>
    </div>
    <div class="col-md-6">
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Projects</h3>
        <div>  
          {{-- <a href="{{ route('dashboard') }}" class="btn-icon-primary lead mr-2"><i class="fas fa-arrow-left"></i></a> --}}
          <a href="{{ route('project.create') }}" class="btn-icon-primary lead"><i class="fas fa-plus"></i></a>
        </div>
      </div>
      <hr>
      @foreach($projects as $project)
      <div class="card {{ $project->category_id == 1 ? "website-card-freecodecamp" : ($project->category_id == 2 ? "website-card-personal" : ($project->category_id == 3 ? "website-card-professional" : "website-card-progress")) }}">
        <div class="card-body">
          <h5 class="card-title">{{ $project->title }}</h5>
          <h6 class="card-subtitle">{{ $project->category->category }}</h6>
          <p class="card-text">{{ substr($project->description, 0, 100) }}...</p>
          <a href="{{ route('project.show', $project->id) }}" class="card-link">Show</a>
          <a href="{{ route('project.edit', $project->id) }}" class="card-link">Edit</a>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

@stop