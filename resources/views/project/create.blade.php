@extends('main')

@section('title', '| Create Project')

@section('content')

<div class="container mt-4 pb-80">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Create Project</h3>
        <a href="{{ route('project.index') }}" class="btn-icon-primary lead"><i class="fas fa-times"></i></a>
      </div>
      <hr>
      <form action="{{ url('project') }}" method="POST">

        {{ csrf_field() }}

        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" name="title" placeholder="Make it catchy" value="{{ old('title') }}">
        </div>
        <div class="form-group">
          <label for="category_id">Category</label>
          <select class="form-control" name="category_id">
            <option>Select</option>
            @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->category }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <textarea class="form-control" name="description" placeholder="Make it brief and to the point" rows="4">{{ old('description') }}</textarea>
        </div>
        <div class="form-group">
          <label for="source">Source Code</label>
          <input type="text" class="form-control" name="source" placeholder="Link to the source code" value="{{ old('source') }}">
        </div>
        <div class="form-group">
          <label for="view">View</label>
          <input type="text" class="form-control" name="view" placeholder="Link to the code in action" value="{{ old('view') }}">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-block btn-outline-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

@stop