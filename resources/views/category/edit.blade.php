@extends('main')

@section('title', '| Edit Category')

@section('content')

<div class="container mt-4 pb-80">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Edit Category</h3>
        <a href="{{ route('project.index') }}" class="btn-icon-primary lead"><i class="fas fa-times"></i></a>
      </div>
      <hr>
      <form action="{{ url('category', $category->id) }}" method="POST">

        {{ method_field('PATCH') }}
        {{ csrf_field() }}

        <div class="form-group">
          <label for="category">Category</label>
          <input type="text" class="form-control" name="category" placeholder="Project type" value="{{ $category->category }}">
          <small id="categoryHelp" class="form-text text-muted">This is basically the subtitle of each Project</small>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-block btn-outline-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

@stop